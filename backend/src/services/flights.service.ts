import { InternalServerError } from 'routing-controllers'
import { UpdateFlightDto } from '../dto/updateFlight.dto'
import { FlightsModel, FlightStatuses } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        try {
            return await FlightsModel.find()
        } catch (e: any) {
            throw new InternalServerError(e)
        }
    }

    async update(code: string, updateFlightDto: UpdateFlightDto) {
        return await FlightsModel.updateOne(
            {
                code,
            },
            updateFlightDto
        )
    }
}
