import { JsonController, Get, Put, Param, Body } from 'routing-controllers'
import { UpdateFlightDto } from '../dto/updateFlight.dto'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('/code/:code')
    async update(
        @Param('code') code: string,
        @Body() updateFlightDto: UpdateFlightDto
    ) {
        return {
            status: 200,
            data: await flightsService.update(code, updateFlightDto),
        }
    }
}
