import { FlightStatuses } from '../models/flights.model'

export class UpdateFlightDto {
    status: FlightStatuses
}
