# Improvements

This is meant to be a list of potential improvements that can be done to the Flights app. This is a non-exhaustive list— there might be additional improvements to be made depending on the functionality that is needed by end users.

I separated suggestions into two categories, although they are not mutually exclusive:

- Tooling & frameworks: Focuses on the use of certain frameworks that would reduce development overhead and simplify buildout and maintenance.
- Code practices: Practices that would drive consistency and reduce the potential of bugs on code.

### Tooling & Frameworks

**NestJS**  
I would suggest using Nest to build the backend. This is primarily because Nest is opinionated on directory composition, which in many ways ensures consistency.
Another good reason is because the Nest tooling has built-in functionality for authentication, database connections, and using ORM/ODMs.

<details>
<summary>An example of how the backend could be structured in NestJS</summary>
<br>

    .
    ├── src                             # Source files (alternatively `source` or `app`)
    |   |── app
    |   |   |-- app.module              # Main module. Imports dependencies used across the project (config, passport, odm/orm)
    |   |── flights
    |   |   |-- flights.module          # Flights logic. Imports dependencies, provides an API controller, and exports services needed by other components.
    |   |   |-- flights.controller
    |   |   |-- flights.service
    |   |── auth
    |       |-- auth.module              # Auth logic. Imports dependencies, provides an auth controller, and exports auth strategies needed by other components.
    |       |-- auth.controller
    |       |-- auth.service
    |       |-- guards
    |       |-- strategies
    ├── docs                            # Auto-generated docs using something like [compodoc](https://compodoc.app/)
    ├── dist                            # Compiled files
    ├── test                            # Automated tests (alternatively `spec` or `tests`)
    ├── utils                           # Utilities that are used across the project
    └── README.md

</details>

**Class-validator**  
Class validator makes it very simple to perform data validation at the endpoint level. It can validate different data types, and integrates well with existing DTOs.
It can also be used at the service level to validate reporting constraints and db query parameter structure.

<details>
<summary>Using class-validator to validate a DTO</summary>
<br>

```typescript
import { IsString, IsDate } from 'class-validator';

export class CreateSecretDto {
    @IsString()
    name: string;

    @IsString()
    value: string;

    @IsDate();
    createdAt: Date;

    @IsString()
    signingKey: string;
}
```

</details>

**Tailwind or Stitches**  
I would suggest using a CSS templating framework like Stitches or Tailwind, which remove the need for inline CSS. They work differently though - Tailwind provides a set of classes that have pre-built CSS stylings, whereas Stitches allows us to produce styled components while using JS code to map them (called CSS in JS). They also work very well with component libraries like MUI.

<details>
<summary>A styled Card component using Stitches</summary>
<br>

```typescript
import { styled } from "../../../styles/theme";

const CardContainer = styled("div", {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  padding: "0px",
  backgroundColor: "$background4", //notice the use of variables. these can be any value - hex colors, px dimensions, etc.
});

const DetailContainer = styled("div", {
  backgroundColor: "$background3",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  flex: "none",
  order: 1,
  flexGrow: 0,
});

interface CardProps {
  imageSrc: string;
  children: React.ReactNode;
}

export default function Card({ imageSrc, children }: CardProps) {
  return (
    <CardContainer>
      <DetailContainer>{children}</DetailContainer>
    </CardContainer>
  );
}
```

</details>

**Passport**  
Passport can be easily integrated with NestJS, and it allows for JWT-based authentication. Additionally, different guards and authentication strategies can be employed depending on the
entity being authentication - so we could have a different authentication strategy for clients than we do for admins. (_Available as @nestjs/passport_)

**NextJS**  
Because of the nature of the project, it lends well to using NextJS's server-side rendering capabilities. The Flights page is a good example, where the initial flights data could be fetched and rendered on the server, using NextJS's `getServerSideProps`, and eventually updated on the client side if flight statuses change.

**Zustand**  
Depending on the needs of the application, Zustand can be used for state management. I personally prefer zustand over redux just because there is less boilerplate code, and zustand has a hooks-based approach. It can also be used in an actions-based approach similar to redux if preferred.

### Code Practices

**Error handling**  
Building consistent error handling across our services would ensure that the client-side can appropriately react to exceptions, and that, most importantly, our application can fail gracefully. This can be done by ensuring proper http status codes are returned on the server, and that an error boundary exists on the frontend.

**Client-Server Authentication**  
If the application is meant to be public, it can still be secured partially by building client-server authentication using JWT. This would ensure that only authorized clients have access to interacting with the API and DB, and would decrease the threat surface of getting DDOS attacks.

**Atomic design pattern**  
While this mainly applies to the frontend, the atomic design pattern is a great way to organize component hierarchies. It divides components into three sections:

- _atoms_ are reusable components; the _units_ of the frontend application
- _molecules_ are components made up of different atoms. They are more complex than atoms and might also involve additional data formatting that is not meant to be present in atoms
- _organisms_ are complex components that are made up of different molecules, and potentially atoms. A good example is the authentication form on a website, which may me made up of a sign up form, log in form, as well as additional text fields.

[More on the atomic design pattern](https://medium.com/@janelle.wg/atomic-design-pattern-how-to-structure-your-react-application-2bb4d9ca5f97)
